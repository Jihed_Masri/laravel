@extends('template')
@section("content")

<section class="hero is-success is-fullheight">
  <div class="hero-head">
<div class="container text-dark bg-white mt-5 pb-2 mb-3">
  <div class="row p-4 bg-mobile">
    <div class="col-md-6">
      <h2 class="text-success">Détails des commandes</h2>
    </div>
  </div>
  @foreach($orders as $item)
  
  <div class="row p-3 effects">
    <div class="col-md-3">
      <a href="detail/{{$item->id}}">
      <img class="img_cart" src="{{ asset('storage/images/' . $item->gallery) }}"></a>
    </div>
    <div class="col-md-3">
      <h4>{{$item->name}}</h4>
      <h5>{{$item->price}} DT</h5>
      <h5>x {{$item->quantite}} </h5>
    </div>
    <div class="col-md-6 spacing_up">
      <a href="status/update/{{$item->order_id}}">
      <input type="checkbox" data-id="{{ $item->id }}" name="status" class="js-switch" {{ $item->statut === "en cours" ? '' : 'checked' }}>
      </a>
      
    </div>
  </div>
  @endforeach
</div>
  </div></section>
@endsection 
