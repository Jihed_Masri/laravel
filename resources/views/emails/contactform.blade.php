@extends('temp')
@section('contenu')


<section class="hero is-success is-fullheight">
  <div class="hero-body">
      <div class="container has-text-centered">
          <div class="column is-4 is-offset-4">
              <h3 class="title has-text-black">Contact Us</h3>
              <div class="box">
                <form action="{{ url('contact') }}" method="POST" >
                  @csrf
                  <div class="field">
                    <div class="form-group">
                      <label class="label">Name</label>
                      <div class="control">
                        <input type="text" class="form-control input is-medium @error('nom') isinvalid @enderror" name="nom" id="nom" placeholder="Votre nom" value="{{
                          old('nom') }}">
                      </div>
                        @error('nom')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                     </div>
                  </div>

                      <div class="field">
                    <div class="form-group">
                      <label class="label">Email</label>
                      <div class="control">
                        <input type="email" class="form-control input is-medium @error('email')
                        is-invalid @enderror" name="email" id="email" placeholder="Votre email" value="{{
                        old('email') }}"></div>
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                     </div>
                  </div>

                  
                      <div class="field">
                    <div class="form-group">
                      <label class="label">Message</label>
                      <div class="control">
                        <textarea class="form-control textarea is-medium @error('message') isinvalid @enderror" name="message" id="message" placeholder="Votre message">{{
                          old('message') }}</textarea>
                      </div>
                        @error('message')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                     </div>
                  </div>

                  <div class="control">
                    <button type="submit" class="button is-link is-fullwidth has-text-weight-medium is-medium">Send Message</button>
                  </div></form>
              </div>
          </div>
      </div>
  </div>
</section>



{{-- <section class="hero is-primary is-fullheight">
    <div class="hero-body">
        <div class="container">
          <div class="columns is-5-tablet is-4-desktop is-3-widescreen">
              <div class="column">
                  <form class="box" action="{{ url('contact') }}" method="POST">
                     
                      @csrf
                        <div class="form-group">
                        <input type="text" class="form-control @error('nom') isinvalid @enderror" name="nom" id="nom" placeholder="Votre nom" value="{{
                        old('nom') }}">
                        @error('nom')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group">
                        <input type="email" class="form-control @error('email')
                        is-invalid @enderror" name="email" id="email" placeholder="Votre email" value="{{
                        old('email') }}">
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group">
                        <textarea class="form-control @error('message') isinvalid @enderror" name="message" id="message" placeholder="Votre message">{{
                        old('message') }}</textarea>
                        @error('message')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        </div>
                      <div class="field">
                        <div class="control col-md-12 col-md-offset-5 spacing_top">
                            <button class="button is-link ">Envoyer !</button>
                            </div>
                          
                        </button>
                      </div>
                  </form>
              </div>
          </div>
        </div>
    </div>
</section> --}}
@endsection


