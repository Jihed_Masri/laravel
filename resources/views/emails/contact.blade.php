<html lang="fr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
<title>Message</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
</head>
<body>
<h2>Prise de contact sur mon beau site</h2>
<p>Réception d'une prise de contact avec les éléments
suivants :</p>
<ul>
<li><strong>Nom</strong> : {{ $contact['nom'] }}
</li>
<li><strong>Email</strong> : {{ $contact['email'] }}
</li>
<li><strong>Message</strong> : {{$contact['message'] }}</li>
</ul>
</body>

