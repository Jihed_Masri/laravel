
@extends('temp')
@section('contenu')
<br>
<section class="hero is-fullheight">
    <div class="hero-head">
        <div class="container has-text-centered">
            <div class="columns is-8 is-variable ">
                <div class="column is-two-thirds has-text-left">
                    <h1 class="title is-1">Contactez-Nous</h1>
                    <p class="is-size-4">Merci. Votre message a été transmis à
                        l'administrateur du site. Vous recevrez une réponse rapidement.</p>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

