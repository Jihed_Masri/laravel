@extends('template')
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Name : {{ $category->name }}</p>

</header>
<div class="card-content">
<div class="content">
<p>Slug : {{ $category->slug }}</p>
</div>
</div>
</div>
@endsection
