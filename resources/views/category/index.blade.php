@extends('template')
@section('css')
<style>
.card-footer {
justify-content: center;
align-items: center;
padding: 0.4em;
}
.btn-info {
margin: 0.3em;
}
</style>
@endsection

@section('content')
{{-- @if(session('info'))
<div class="alert alert-success">
{{ session('info') }}
</div>
@endif --}}
 @if(session()->has('info'))
<script>
toastr.success(session('info'));
</script>
@endif 

<div class="card">
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">categories</p>
 <a class="btn btn-info" href="{{ route('categories.create') }}">Créer une categorie</a>
{{--<a class="btn btn-info" href="{{ route('films.index') }}">Films</a>
<a class="btn btn-info" href="{{ route('categories.index') }}">Categories</a> --}}
</header>
<div class="card-content">
<div class="content">
<table class="table is-hoverable">
<thead>
<tr>
<th>Name</th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
    @foreach($categories as $category)
<tr>
<td><strong>{{ $category->name}}</strong></td>
<td><a class="btn btn-primary" href="{{ route('categories.show', $category->id)}}">Voir</a></td>
<td><a class="btn btn-warning" href="{{ route('categories.edit', $category->id)}}">Modifier</a></td>
<td>
<form action="{{route('categories.destroy', $category->id) }}" method="post">
@csrf
@method('DELETE')
<button class="btn btn-danger" type="submit">Supprimer</button>
</form>
</td>
</tr>
@endforeach
 



    
</tbody>
</table>
</div>
</div>
</div>

@endsection