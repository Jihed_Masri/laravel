@extends('template')
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Création d'une categorie</p>
</header>
<div class="card-content">
<div class="content">
<form action="{{ route('categories.store') }}" method="POST">
@csrf
<div class="field">
<label class="label">Name</label>
<div class="control">
<input class="input" type="text" name="name" value="{{ old('name') }}" placeholder="nom du categorie">
</div>
@error('name')
<p class="help is-danger">{{ $message }}</p>
@enderror
</div>
<div class="field">
    <label class="label">Slug</label>
    <div class="control">
    <input class="input @error('slug') is-danger @enderror" type="text" name="slug" value="{{ old('slug') }}" placeholder="Slug du categorie">
    </div>
    @error('slug')
    <p class="help is-danger">{{ $message }}</p>
    @enderror
    
    </div>
<div class="field">
<div class="control">
<button class="button is-link">Envoyer</button>
</div>
</div>
</form>
</div>
</div>
</div>
@endsection