@extends('template')
@section("content")
<section class="hero is-fullheight is-default is-bold">
    <div class="hero-head">
        <div class="container has-text-centered">
            <div class="columns is-vcentered">
                <div class="column is-5">
                    <figure class="image is-4by3">
                        <img src="{{ asset('storage/images/' . $livre->gallery) }}">
                    </figure>
                </div>
                <div class="column is-6 is-offset-1">
                    <h1 class="title is-2">
                        {{$livre['name']}}
                    </h1>
                    <h3 class="subtitle is-4">Prix : {{$livre['price']}} DT</h3>
                    <h4 class="subtitle is-4">Année: {{$livre['year']}}</h4>
                    <h4 class="subtitle is-4">description: {{$livre['description']}}</h4>
                    <h4 class="subtitle is-4">Auteur: {{$author}}</h4>
                    <h3 class="subtitle is-4">Categories :</h3>
                        @foreach($livre->categories as $category)
                        <h4 class="subtitle is-4">+{{ $category->name }}</h4>
                        @endforeach
                    <br>
                    <br>
                    @if(Session::has('user'))
                     @if (session('user')->role =="")
                    <p class="has-text-centered">
                        <form action="/add_to_cart" method="POST">
                            @csrf
                            <input type="hidden" name="livre_id" value={{$livre['id']}}>
                        <button class="button is-medium is-info is-outlined">Add to Cart</button>
                        </form>
                    </p>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection