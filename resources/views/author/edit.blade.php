@extends('template')
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Modification d'une auteur</p>
</header>
<div class="card-content">
<div class="content">
<form action="{{ route('authors.update', $author->id) }}"
method="POST">
@csrf
@method('put')
<div class="field">
<label class="label">Nom</label>
<div class="control">
<input class="input @error('name') is-danger @enderror"
type="text" name="name" value="{{ old('name', $author->name) }}"
placeholder="Nom du auteur">
</div>
@error('name')
<p class="help is-danger">{{ $message }}</p>
@enderror
</div>

<div class="field">
<label class="label">slug</label>
<div class="control">
<textarea class="textarea" name="slug"
placeholder="slug">{{ old('slug', $author->slug) }}
</textarea>
</div>
@error('slug')
<p class="help is-danger">{{ $message }}</p>
@enderror
</div>
<div class="field">
<div class="control">
<button class="button is-link">Envoyer</button>
</div>
</div>
</form>
</div>
</div>
</div>
@endsection