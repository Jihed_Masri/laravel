@extends('template')
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Name : {{ $author->name }}</p>
</header>
<div class="card-content">
<div class="content">
<p>Slug : {{ $author->slug }}</p>
</div>
</div>
</div>
@endsection
