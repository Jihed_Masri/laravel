@extends('template')
@section('css')
<style>
.card-footer {
justify-content: center;
align-items: center;
padding: 0.4em;
}
.btn-info {
margin: 0.3em;
}
</style>
@endsection
@section('content')
@if(session()->has('info'))
<div class="alert alert-success">
{{ session('info') }}
</div>
@endif
<div class="card">
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Auteurs</p>
<a class="btn btn-info" href="{{ route('authors.create') }}">Ajouter une Auteur</a>
</header>
<div class="card-content">
<div class="content">
<table class="table is-hoverable">
<thead>
<tr>
<th>Name</th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
    @foreach($authors as $author)
<tr>
<td><strong>{{ $author->name}}</strong></td>
<td><a class="btn btn-primary" href="{{ route('authors.show', $author->id)}}">Voir</a></td>
<td><a class="btn btn-warning" href="{{ route('authors.edit', $author->id)}}">Modifier</a></td>
<td>
<form action="{{route('authors.destroy', $author->id) }}" method="post">
@csrf
@method('DELETE')
<button class="btn btn-danger" type="submit">Supprimer</button>
</form>
</td>
</tr>
@endforeach

    
</tbody>
</table>
</div>
</div>
</div>
@endsection