<?php 
use App\Http\Controllers\LivreController;
$total=0;
if(Session::has('user'))
{
  $total= LivreController::cartItem();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,
initial-scale=1">
<title>Films</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0- 
     alpha/css/bootstrap.css" rel="stylesheet">
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" 
     href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
   


@yield('css')
</head>
<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
          <a class="navbar-item" href="/">
            <img src="{{ asset('storage/images/bulma-logo.png') }}"width="112" height="30">
            {{-- <img src="https://drive.google.com/file/d/1zsX6AQ2SPKIPJJz7IVcwcAhLjTrsHspu/view.png" width="112" height="28"> --}}
          </a>
      
          <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
      
        <div id="navbarBasicExample" class="navbar-menu">
          <div class="navbar-start">
            @if(Session::has('user'))
            @if (session('user')->role =="")
            <a class="navbar-item" href="/orders">
              commandes

            </a>
      
            <a class="navbar-item" href="/contact">
              contact us
            </a>
            @endif
            @if (session('user')->role =="admin")           
           
            <div class="navbar-item has-dropdown is-hoverable">
              <a class="navbar-link">
                Admin
              </a>
              <div class="navbar-dropdown">
                <a class="navbar-item" href="{{ route('livres.index') }}">
                  Gérer livres
                </a>
                <a class="navbar-item" href="{{ route('categories.index') }}">
                  Gérer Categories
                </a>
                <a class="navbar-item" href="{{ route('authors.index') }}">
                  Gérer Auteurs
                </a>
                <hr class="navbar-divider">
                 
                <a class="navbar-item" href="/allorders">
                  commandes
                </a>
                @endif
                @endif 
              </div>
            </div>
          </div>
          @if(Session::has('user'))  
          <div class="navbar-end">
            <div class="navbar-item">
              <div class="buttons">
                @if (session('user')->role =="")
               <a class="spacing_cart" id="cart"href="/cart"><i  class="glyphicon glyphicon-shopping-cart"></i> Panier <span class="badge">{{$total}}</span></a>
               @endif
                {{-- <a class="button is-primary" href="{{route('logout')}}">
                    <span class="glyphicon glyphicon-log-out spacing_right"></span><strong> Logout</strong>
                </a> --}}
                <form method="POST" action="{{route('logout')}}" class="button is-primary">
                  @csrf
                  
                  <button type="submit"><span class="glyphicon glyphicon-log-out spacing_right"></span><strong> Déconnexion</strong></button>
              </form>
              </div>
            </div>
          </div>
          @else 
          <div class="navbar-end">
            <div class="navbar-item">
              <div class="buttons">
                <a class="button is-primary" href="{{route('register')}}">
                    <span class="glyphicon glyphicon-user spacing_right"></span><strong> Register</strong>
                </a>
                <a href="{{route('login')}}" class="button is-light">
                    <span  class="glyphicon glyphicon-log-in spacing_right"></span><strong> Log in </strong>
                </a>
              </div>
            </div>
          </div>

          @endif

        </div>
      </nav>
<main class="section">
<div class="container">
 
@yield('content')
</div>
</main>
{{-- {!! Toastr::message() !!} --}}
</body>

<style>
  button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
  .img_cart{
      width: 35%;
      }
 a:hover, a:visited, a:link, a:active
{
    text-decoration: none;
}
a {
    color: rgb(0, 0, 0);
    text-decoration: none;
}

a:hover 
{
     color:#00A0C6; 
     text-decoration:none; 
     cursor:pointer;  
}
    .spacing_right{margin-right: 7px;}
    .spacing_up{margin-top: 17px;}
    .spacing_cart{margin-right: 18px;margin-top: -5px;}
    .spacing_top{margin-top: 27px;}
    .custom-login{
        height: 500px;
        padding-top: 100px;
    }
    img.slider-img{
        height: 400px !important
    }
    .custom-product{
        height: 600px
    }
    .slider-text{
        background-color: #35443585 !important;
    }
    .trending-image{
        height: 100px;
    }
    .trening-item{
        float: left;
        width: 30%;
        margin-right:25px;
    }
    .trending-wrapper{
        margin: 30px;
    }
    .detail-img{
        height: 200px;
    }
    .search-box{
        width: 500px !important
    }
    .cart-list-devider{
        border-bottom: 1px solid #ccc;
        margin-bottom: 20px;
        padding-bottom: 20px
    }
    html,body {
  font-family: 'Questrial', sans-serif;
  font-size: 14px;
  font-weight: 300;
}
.hero.is-success {
  background: #F2F6FA;
}
.hero .nav, .hero.is-success .nav {
  -webkit-box-shadow: none;
  box-shadow: none;
}
.box {
  margin-top: 5rem;
}
.avatar {
  margin-top: -70px;
  padding-bottom: 20px;
}
.avatar img {
  padding: 5px;
  background: #fff;
  border-radius: 50%;
  -webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
  box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
}
input {
  font-weight: 300;
}
p {
  font-weight: 700;
}
p.subtitle {
  padding-top: 1rem;
}

.login-hr{
  border-bottom: 1px solid black;
}

.has-text-black{
  color: black;
}

.field{
  padding-bottom: 10px;
}

.fa{
  margin-left: 5px; 
}
$main-color: #6394F8;
$light-text: #ABB0BE;

@import url(https://fonts.googleapis.com/css?family=Lato:300,400,700);

@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css);

*, *:before, *:after {
  box-sizing: border-box;
}

.lighter-text {
  color: #ABB0BE;
}
.containe {
  margin: auto;
  width: 80%;
}
.main-color-text {
  color: $main-color;
}


  
  .navbar-right {
    float: right;
  }
  ul {
    
    li {
      display: inline;
      padding-left: 20px;
      a {
        color: #777777;
        text-decoration: none;
        
        &:hover {
          color: black;
        }
      }
    }
  }
}


.badge {
    background-color: #6394F8;
    border-radius: 10px;
    color: white;
    display: inline-block;
    font-size: 12px;
    line-height: 1;
    padding: 3px 7px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}

.shopping-cart {
  margin: 20px 0;
  float: right;
  background: #F2F6FA;
  width: 320px;
  position: relative;
  border-radius: 3px;
  padding: 20px;
}
  
  .shopping-cart-header {
    border-bottom: 1px solid #E8E8E8;
    padding-bottom: 15px;
  }
    .shopping-cart-total {
      float: right;
    }
  }
  
  .shopping-cart-items {
    
    padding-top: 20px;

      li {
        margin-bottom: 18px;
      }

    img {
      float: left;
      margin-right: 12px;
    }
    
    .item-name {
      display: block;
      padding-top: 10px;
      font-size: 16px;
    }
    
    .item-price {
      color: $main-color;
      margin-right: 8px;
    }
    
    .item-quantity {
      color: $light-text;
    }
  }
   
}

.shopping-cart:after {
	bottom: 100%;
	left: 89%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
	border-bottom-color: white;
	border-width: 8px;
	margin-left: -8px;
}

.cart-icon {
  color: #515783;
  font-size: 24px;
  margin-right: 7px;
  float: left;
}
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
/*
h4{
       color: #000;
      padding: 0;
      margin: 0;
      }
      h3{
      padding: 0;
      margin: 0;
      font-size: 22px;
      }
      h5{
      font-size: 16px;
      padding-top: 5px;
      }
      h4 span{
      font-size: 20px !important;
      }
      p{
      color: #2E86C1;
      margin-bottom: 5px;
      font-size: 14px;
      }
      ul{
      list-style-type: none;
      }
      ul li a{
      height: 10px;
      width: 10px;
      position: absolute;
      background: red;
      }
      ul li a:nth-child(3){
      background: green;
      } */
      .btn-custom{
      background:#DC7633;
      color: #fff; 	
      }
      .btn-custom:hover{
      background:#E59866;
      color: #fff; 	
      }
      .color-group {
      width: 5%;
      float: left;
      display: inline-block;
      display: -webkit-inline-box;
      padding-top: 5px;
      }
      
      .color-block {
      float: left;
      position: relative;
      width: 100%;
      margin: 1px;
      padding-bottom: 100%;
      }
      .fa{
      font-size: 30px;
      padding: 10px;
      }
      .fa-edit{
      color: #007bff;
      }
      .fa-trash-o{
      color: #dc3545;
      }
      .effects{
      border: 1px solid #fff;
      background: #fff;
      color: #888f96;
      width: 93%;
      margin-left: 30px;
      margin-top: 20px;
      }
      .effects:hover{
      background: #fff;
      transition: .4s;
      color: #000;
      cursor: pointer;
      transform: scale(1.03);
      border: 1px solid #f7f3f3;
      border-radius: 8px;
      }
      @media(max-width: 768px){
      .effects{
      text-align: center;
      margin: 20px;
      }
      .color-group{
      margin-right: 27%;
      float: none;
      text-align: center;
      }
       h2{
      text-align: center;
      } 
      .pull-right{
      float: none;
      text-align: center;
      display: block;
      margin: 20px auto;
      }
      }
      @media (max-width: 576px){
      .container {
      max-width: 90%;
      }
      .effects{
      background: #e8e8e852;
      border-radius: 8px;
      margin: 10px;
      }
      .col-md-3 h4:nth-child(1){
      padding-top: 10px;
      font-size: 20px;
      }
      .fa{
      padding: 5px;
     
      }
      img{
      width: 45%;
      }
      h5{
      font-size: 14px;
      }
      h3{
      font-size: 15px;
      }
      .color-group{
      padding-top: 10px;
      padding-bottom: 10px;
      }
      }


</style>

<script>let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

  elems.forEach(function(html) {
      let switchery = new Switchery(html,  { size: 'small' });
  });
  
  $(".modal-button").click(function() {
            var target = $(this).data("target");
            $("html").addClass("is-clipped");
            $(target).addClass("is-active");
         });
         
         $(".modal-close").click(function() {
            $("html").removeClass("is-clipped");
            $(this).parent().removeClass("is-active");
         });

</script>
@jquery
    @toastr_js
    @toastr_render
</html>


