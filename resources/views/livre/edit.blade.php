@extends('template')
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">Modification d'un livre</p>
</header>
<div class="card-content">
<div class="content">
<form action="{{ route('livres.update', $livre->id) }}"method="POST" enctype="multipart/form-data">
@csrf
@method('put')
<div class="field">
<label class="label">Nom</label>
<div class="col-md-offset-4 col-xs-4">
<input class="input @error('name') is-danger @enderror"
type="text" name="name" value="{{ old('name', $livre->name) }}"
placeholder="Titre du livre">
</div>
@error('name')
<p class="help is-danger">{{ $message }}</p>
@enderror
</div><br>
<div class="field">
    <label class="label">Auteur</label>
    <div class="select col-md-offset-5 ">
    <select name="author_id" >
        @foreach($authors as $author)        
        <option value="{{ $author->id }}" {{ $author->id ==$livre->Author_id ? 'selected' : ''}}>{{ $author->name }}</option>
        @endforeach
    </select>
    </div>
    </div><br>
<div class="field">
    <label class="label">Categories</label>
    <div class="select is-multiple  col-md-offset-5" >
    <select name="CategoriesTable[]" multiple>
    @foreach($categories as $category)
    <option value="{{ $category->id }}" {{ in_array($category->id,old('CategoriesTable') ?: $livre->categories->pluck('id')->all()) ? 'selected' : '' }}>{{$category->name }}</option>
    @endforeach
    </select>
    </div>
    </div><br>
    <div class="field">
        <label class="label">Prix</label>
        <div class="col-md-offset-4 col-xs-4">
        <input class="input" name="price"value="{{ old('price', $livre->price) }}"
        placeholder="Prix du livre">

        </div>
        @error('price')
        <p class="help is-danger">{{ $message }}</p>
        @enderror
        </div><br>
        <div class="field">
            <label class="label">Année</label>
            <div class="control col-md-offset-4 col-xs-4">
            <input class="input" name="year"value="{{ old('year', $livre->year) }}"
            placeholder="Année de publication">
 
            </div>
            @error('year')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
            </div><br>
<div class="field">
<label class="label">Description</label>
<div class="control col-md-offset-4 col-xs-4">
<input class="input" name="description"value="{{ old('description', $livre->description) }}"
placeholder="Description du livre">

</div>
@error('description')
<p class="help is-danger">{{ $message }}</p>
@enderror
</div>
    <div class="control col-md-offset-4 col-xs-4">

        <label class="label">Image </label>
         <input type="file" name="gallery" class="form-control" placeholder="Post Title">
        @error('gallery')
          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
       @enderror
</div>
<div class="field">
<div class="control">
    <div class="control col-md-12 col-md-offset-5 spacing_top">
        <button class="button is-link">Envoyer</button>
        </div>

</div>
</div>
</form>
</div>
</div>
</div>
@endsection