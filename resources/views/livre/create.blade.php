@extends('template')
@section('content')
<div class="card" >
<header class="card-header">

<div class="container">
    <div class="row">
      <div class="col-md-12">
          <div class="pull-left">
            <p class="card-header-name">Création d'un livre :</p>
              </div>
          <div class="pull-right">
          </div>
      </div>
    </div>  
  </div>



</header>
<div class="card-content">
<div class="content">
<form action="{{ route('livres.store') }}" method="POST" enctype="multipart/form-data">
@csrf
<div class="field ">
    <label class="label">Nom</label>
    <div class="col-md-offset-4 col-xs-4">
    <input class="input @error('name') is-danger @enderror" type="text" name="name" value="{{ old('name') }}" placeholder="Nom du livre">
    </div>
    @error('name')
    <p class="help is-danger">{{ $message }}</p>
    @enderror
    </div><br>
<div class="field">
    <label class="label">Auteur</label>
<div class="select col-md-offset-5 ">
<select name="author_id" >
@foreach($authors as $author)
<option value="{{ $author->id }}">{{ $author->name }}
</option>
@endforeach
</select>
</div>
<label class="label">Categories</label>
<div class="select is-multiple  col-md-offset-5" >
<select name="CategoriesTable[]" multiple data-width="75%">
@foreach($categories as $category)
<option value="{{ $category->id }}" {{ in_array($category->id, old('CategoriesTable') ?: []) ? 'selected' : '' }}>{{ $category->name }}</option>
@endforeach
</select>
</div> 

<div class="field">
    <div class="col-md-offset-4 col-xs-4">
        <label class="label">prix</label>
    <input class="input @error('price') is-danger @enderror" type="text" name="price" value="{{ old('price') }}" placeholder="Prix du livre">
    </div>
    @error('price')
    <p class="help is-danger">{{ $message }}</p>
    @enderror
    </div>
    <div class="field">
        
        <div class="control col-md-offset-4 col-xs-4">
            <label class="label">Année</label>
        <input class="input @error('year') is-danger @enderror" type="text" name="year" value="{{ old('year') }}" placeholder="Année de publication">
        </div>
        @error('year')
        <p class="help is-danger">{{ $message }}</p>
        @enderror
        </div> 
        <div class="field">
            
            <div class="control col-md-offset-4 col-xs-4">
                <label class="label">Description</label>
            <input class="input @error('description') is-danger @enderror" type="text" name="description" value="{{ old('description') }}" placeholder="description">
            </div>
            @error('description')
            <p class="help is-danger">{{ $message }}</p>
            @enderror
            </div> 
            <div class="field">
{{--                
                <div class="control">
                    <label class="label">Image</label>
                <input class="input @error('gallery') is-danger @enderror" type="text" name="gallery" value="{{ old('gallery') }}" placeholder="image">
                </div>
                @error('gallery')
                <p class="help is-danger">{{ $message }}</p>
                @enderror
                </div>              --}}
                <div class="control col-md-offset-4 col-xs-4">

                        <label class="label">Image </label>
                         <input type="file" name="gallery" class="form-control" placeholder="Post Title">
                        @error('gallery')
                          <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                       @enderror
                </div>
<div class="field ">
<div class="control col-md-12 col-md-offset-5 spacing_top">
<button class="button is-link">Ajouter</button>
</div>

</div>

</form>
</div>
</div>
</div>
@endsection