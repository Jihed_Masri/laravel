@extends('template')
@section('css')
<style>
.card-footer {
justify-content: center;
align-items: center;
padding: 0.4em;
}
.btn-info {
margin: 0.3em;
}
select, .is-info {
margin: 0.3em;
}


</style>
@endsection
{{-- @section("nav")
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">Book Store</a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>
@endsection --}}
@section('content')
<div class="card">
@section('content')
<div class="card">
<header class="card-header">
<p class="card-header-title">livres</p>
{{-- <div class="select">
    <select onchange="window.location.href = this.value">
    <option value="{{ route('livres.index') }}" @unless($slug) selected
    @endunless>Toutes catégories</option>
    @foreach($categories as $category)
    <option value="{{ route('livres.category', $category->slug) }}"
    {{ $slug == $category->slug ? 'selected' : '' }}>{{ $category->name }}</option>
    @endforeach
    </select>
    </div> --}}
    
<a class="btn btn-info" href="{{ route('livres.create') }}">Créer un livre</a>
{{-- <a class="btn btn-info" href="{{ route('categories.index') }}">Categories</a>
<a class="btn btn-info" href="{{ route('actors.index') }}">Acteurs</a> --}}
</header>
<div class="card-content">
<div class="content">
<table class="table is-hoverable">
<thead>
<tr>
<th>Titre</th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
    @foreach($livres as $livre)
    {{-- <tr @if($livre->deleted_at) class="has-background-grey-lighter" @endif> --}}
    <td><strong>{{ $livre->name }}</strong></td>
    <td>
    {{-- @if($livre->deleted_at)
    <form action="{{ route('livres.restore', $livre->id) }}"
    method="post">
    @csrf
    @method('PUT')
    <button class="btn btn-primary"
    type="submit">Restaurer</button>
    </form>
    @else --}}
    <a class="btn btn-primary" href="{{ route('livres.show',$livre->id) }}">Voir</a>
    {{-- @endif --}}
    </td>
    <td>
    {{-- @if($livre->deleted_at)
    @else --}}
    <a class="btn btn-warning" href="{{ route('livres.edit',
    $livre->id) }}">Modifier</a>
    {{-- @endif --}}
    </td>
    <td>
        <form action="{{
            route('livres.destroy', $livre->id) }}" method="post">
            @csrf
            @method('DELETE')
            <button
            class="button is-danger" type="submit">Supprimer</button>
            </form>
            
    </td>
    </tr>
    @endforeach
    
</tbody>
</table>
</div>
</div>
</div>
@endsection