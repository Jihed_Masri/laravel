<?php 
use App\Http\Controllers\LivreController;
$total=0;
if(Session::has('user'))
{
  $total= LivreController::total();
}

?>
@extends('template')
@section("content")

<section class="hero is-success is-fullheight">
  <div class="hero-head">
<div class="container text-dark bg-white mt-5 pb-2 mb-3">
  <div class="row p-4 bg-mobile">
    <div class="col-md-6">
      <h2 class="text-success">Détails de panier</h2>
      <h2 class="text-primary spacing_up">Prix total :{{$total}} DT</h2>
    </div>
    <div class="col-md-6 "> 
      <form action="/Passeorder" method="POST" >
        @csrf
      <button type="submit" class="btn btn-success pull-right">Passez commande</button>
      </form>

    </div>
  </div>
  @foreach($livres as $item)
  
  <div class="row p-4 effects">
    <div class="col-md-3">
      <a href="detail/{{$item->id}}">
      <img class="img_cart" src="{{ asset('storage/images/' . $item->gallery) }}"></a>
    </div>
    <div class="col-md-2 spacing_up">
      <h4>{{$item->name}}</h4>
      <h5>{{$item->price}} DT</h5>
    </div>
   

        <div class="col-sm-2 spacing_up">
          <form action="" method="POST">
            <div class="input-group">
                <span class="input-group-btn">
                  <a href="/decrease/{{$item->carts_id}}"> 
              <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[1]">
                  <span class="glyphicon glyphicon-minus"></span>
                </button></a>
                </span>
                <input type="text" name="quant[1]" class="form-control input-number" value={{$item->carts_quantite}} min="1" >
                <span class="input-group-btn">
                  <a href="/increase/{{$item->carts_id}}"> 
              <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                  <span class="glyphicon glyphicon-plus"></span>
                </button></a>
                </span>
            </div>
          </form>
    </div>

    <div class="col-md-5 spacing_up">    
      <a href="/removecart/{{$item->carts_id}}"> 
      <button type="button" class="btn btn-danger ">Supprimer</button></a>
    </div>
  </div>
  @endforeach
</div>
  </div>
</section>
@endsection 