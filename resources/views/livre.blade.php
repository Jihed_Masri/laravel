@extends('template')
@section("content")
<div class="container">
  <div class="section">
    <div id="app" class="row columns is-multiline ">
      @foreach($livres as $item)
     
      <div class="trening-item spacing_up">
        <div class="card is-shady">
          <div class="card-image">
            <figure class="image is-3by4">
              <img src="{{ asset('storage/images/' . $item->gallery) }}">
            </figure>
          </div>
          <div class="card-content">
            <div class="content">
              <h4>{{$item['name']}}</h4>
              <a href="{{ route('livres.show',$item->id) }}">
              <span class="button is-link modal-button col-md-offset-5" data-target="modal-card">détails</span></a>
            </div>
          </div>
        </div>
        </div>
      
      {{-- <div class="trening-item">
        <div class="card large">
          <div class="card-image">
            <figure class="image is-16by9">
              <img src="{{ asset('storage/images/' . $item->gallery) }}">
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-content">
                <a href="detail/{{$item['id']}}">
                <p class="title is-4 no-padding">{{$item['name']}}</p></a>
              </div>
            </div>
          </div>
        </div> --}}
        @endforeach
      </div>
      
    </div>
  </div>
</div>

  {{-- <div class="trening-item">
  <div class="card is-shady">
    <div class="card-image">
      <figure class="image is-4by3">
        <img src="https://source.unsplash.com/6Ticnhs1AG0" alt="Placeholder image">
      </figure>
    </div>
    <div class="card-content">
      <div class="content">      
  
        <span class="button is-link modal-button" data-target="modal-card">Modal Card</span>
      </div>
    </div>
  </div>
  </div> --}}
  {{-- <div class="trening-item">
    <div class="card is-shady">
      <div class="card-image">
        <figure class="image is-4by3">
          <img src="https://source.unsplash.com/6Ticnhs1AG0" alt="Placeholder image">
        </figure>
      </div>
      <div class="card-content">
        <div class="content">
         
          <span class="button is-link modal-button" data-target="modal-card">Modal Card</span>
        </div>
      </div>
    </div>
    </div>
    <div class="trening-item">
      <div class="card is-shady">
        <div class="card-image">
          <figure class="image is-4by3">
            <img src="https://source.unsplash.com/6Ticnhs1AG0" alt="Placeholder image">
          </figure>
        </div>
        <div class="card-content">
          <div class="content">
            
            <span class="button is-link modal-button" data-target="modal-card">Modal Card</span>
          </div>
        </div>
      </div>
      </div> --}}


@endsection