<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LivreController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\LivreAdminController;
use App\Http\Controllers\CategoryAdminController;
use App\Http\Controllers\AuthorAdminController;
use App\Http\Controllers\ContactController;
use App\Models\{Livre, Category,Author};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get("/",[LivreController::class,'index']);
Route::get("detail/{id}",[LivreController::class,'detail']);
Route::post("add_to_cart",[LivreController::class,'addToCart']);
Route::get("cart",[LivreController::class,'cartList']); 
Route::get("removecart/{id}",[LivreController::class,'removeCart']); 
Route::get("increase/{id}",[LivreController::class,'increaseQuantite']); 
Route::get("decrease/{id}",[LivreController::class,'decreaseQuantite']); 
// Route::get('/email',[EmailController::class,'create']);
// Route::post('send.email',[EmailController::class,'sendEmail']);
//Route::post('/email', 'EmailController@sendEmail')->name('send.email');
Route::get('contact', [ContactController::class, 'create']);
Route::post('contact', [ContactController::class, 'store']);

Route::resource('livres', LivreAdminController::class);
Route::resource('categories', CategoryAdminController::class);
Route::resource('authors', AuthorAdminController::class);
Route::get('author/{slug}/livres', [LivreAdminController::class, 'index'])->name('livres.author');
Route::get('/livres/{id}/category',function ($id){return Livre::find($id)->categories;});

Route::post("/Passeorder",[LivreController::class,'Passeorder']);
Route::get("orders",[LivreController::class,'orders']);
Route::get("allorders",[LivreController::class,'allorders']);
Route::get('/status/update/{id}', [LivreController::class,'updateStatus']);
