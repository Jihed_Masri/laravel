<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Livre, Author, Category};
use App\Http\Requests\Livre as LivreRequest;

class LivreAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($slug = null)
{
     if(session()->has('info'))
    {
        toastr()->success(session('info'));session()->forget('info');}
$query = $slug ? Author::whereSlug($slug)->firstOrFail()->livres() :
Livre::query();
// $livres = $query->withTrashed()->oldest('title')->paginate(5);
$authors = Author::all();
$livres = Livre::all();
return view('livre/index', compact('livres', 'authors', 'slug'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $authors = Author::all();
        $categories = Category::all();
        // if(session()->has('info'))
        // {
        //     toastr()->success(session('info'));session()->forget('info');
        // }
        return view('livre/create', compact('authors','categories'));

        }
        

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LivreRequest $livreRequest)
{
// livre::create($livreRequest->all());
// return redirect()->route('livres.index')->with('info', 'Le livre a bien été créé');
if($livreRequest->hasFile('gallery')){
    $file = $livreRequest->file('gallery');
    $ext = $livreRequest->file('gallery')->getClientOriginalExtension();
    $imageName = time().'.'.$ext;  
    $file->storeAs('public/images',$imageName);
   
}else {$imageName="noImage.png";}
$livre = Livre::create($livreRequest->all());
$livre->gallery = $imageName;
$livre->save();   
$livre->categories()->attach($livreRequest->CategoriesTable);
$livreRequest->session()->put('info', 'Le livre a bien été créé');
return redirect()->route('livres.index');
}



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Livre $livre)
        {
         $author=Author::find($livre->Author_id)->name;      
         $livre->with('categories')->get();
         return view('livre/show', compact('livre', 'author'));
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Livre $livre)
{$authors = Author::all();
    $id=$livre->Author_id;
return view('livre/edit', compact('livre','authors','id'));
}


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LivreRequest $livreRequest, Livre $livre)
    {
        $livre->update($livreRequest->all());
        if($livreRequest->hasFile('gallery')){
            $file = $livreRequest->file('gallery');
            $ext = $livreRequest->file('gallery')->getClientOriginalExtension();
            $imageName = time().'.'.$ext;  
            $file->storeAs('public/images',$imageName);  
            $livre->update(['gallery' => $imageName]);
        }
      $livre->categories()->sync($livreRequest->CategoriesTable);
      $livreRequest->session()->put('info', 'Le livre a bien été modifié');
    return redirect()->route('livres.index');
    

  
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Livre $livre)
        {
        $livre->delete();
        $request->session()->put('info', 'Le livre a bien été supprimé dans la base de données.');
        return redirect()->route('livres.index');
        }
        
        
}
