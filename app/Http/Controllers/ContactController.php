<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
class ContactController extends Controller
{
public function create()
{
return view('emails/contactform');
}
public function store(Request $request)
{
$validator = Validator::make($request->all(), [
'nom' => 'bail|required|between:5,20|alpha',
'email' => 'bail|required|email',
'message' => 'bail|required|max:250'
]);
if ($validator->fails()) {
return back()->withErrors($validator)->withInput();
}
Mail::to('administrateur@chezmoi.com')
->send(new Contact($request->except('_token')));
return view('emails/confirm');
}
}






