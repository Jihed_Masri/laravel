<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Livre, Author, Category};
use App\Http\Requests\Author as AuthorRequest;
class AuthorAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {$authors = Author::all();
        if(session()->has('info'))
{
    toastr()->success(session('info'));session()->forget('info');
}
        return view('author/index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $authorRequest)
    {      
        Author::create($authorRequest->all());
        $authorRequest->session()->put('info', 'auteur a bien été créé');
        return redirect()->route('authors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function show(Author $author)
{
return view('author/show', compact('author'));
}
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
        {
        return view('author/edit', compact('author'));
        }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $authorRequest, Author $author)
{
$author->update($authorRequest->all());
$authorRequest->session()->put('info', 'auteur a bien été modifié');
return redirect()->route('authors.index');
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Author $author)
    {
        $author->delete();
        $request->session()->put('info', 'auteur a bien été supprimé dans la base de données.');
        return redirect()->route('authors.index');
    }
}
