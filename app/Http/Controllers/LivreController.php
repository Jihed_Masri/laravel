<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Order;
use App\Models\{Livre, Author, Category};
use Illuminate\Database\Query\Expression;
use Session;
use Illuminate\Support\Facades\DB;
class LivreController extends Controller
{
    function index()
    {
        $data= Livre::all();
        if(session()->has('info'))
        {
            toastr()->success(session('info'));session()->forget('info');}
       return view('livre',['livres'=>$data]);
    }
    function detail(Livre $livre)
    {
        // $livre =Livre::find($id);
        // $livre->with('categories')->get();
        // $livre->with('categories')->get();
        // $author=Author::find($livre->Author_id)->name;
        // return view('detail', compact('livre', 'author'));
    }
    function addToCart(Request $req)
    {
        if($req->session()->has('user'))
        {
            if (Cart::where('livre_id', '=', $req->livre_id)
                    ->where('user_id', '=', session()->get('user')['id'])
                    ->exists()) {
                        $cart= DB::table('carts')
                        ->where('livre_id', '=', $req->livre_id)
                        ->where('user_id', '=', session()->get('user')['id'])
                        ->update(['quantite' =>new Expression('quantite + 1')]);
                        return redirect('/');  
             }
             else{
           $cart= new Cart;
           $cart->user_id=$req->session()->get('user')['id'];
           $cart->livre_id=$req->livre_id;
           $cart->quantite=1;
           $cart->save();
           return redirect('/');}

        }
        else
        {
            return redirect('/login');
        }
    }
    static function total()
    {static $total = 0;
     $userId=Session::get('user')['id'];
     $UserCart= Cart::where('user_id',$userId)->get();
     foreach($UserCart as $cart)
         {$quantite=$cart->quantite;
          $livre=Livre::find($cart->livre_id)->price; 
          $total = $total +$quantite*$livre;
         }
      
     return $total;
    }
    static function cartItem()
    {
     $userId=Session::get('user')['id'];
     return Cart::where('user_id',$userId)->sum('quantite');
    }
    function cartList()
    {
       $userId=Session::get('user')['id'];
       $livres= DB::table('carts')
        ->join('livres','carts.livre_id','=','livres.id')
        ->where('carts.user_id',$userId)
        ->select('livres.*','carts.id as carts_id','carts.quantite as carts_quantite')
        ->get();

        return view('cartlist',['livres'=>$livres]);
    }
    function removeCart($id)
    {
        Cart::destroy($id);
        return redirect('cart');
    }
    function increaseQuantite($id)
    {
        Cart::where('id',$id)->update(['quantite' =>new Expression('quantite + 1')]);
        return redirect('/cart');
    }
    function decreaseQuantite($id)
    {   $cart =Cart::find($id);
        if ($cart->quantite>1){
        Cart::where('id',$id)->update(['quantite' =>new Expression('quantite - 1')]);}
        return redirect('/cart');
    }
    // function orderNow()
    // {
    //     $userId=Session::get('user')['id'];
    //     $total= $products= DB::table('carts')
    //      ->join('products','cart.product_id','=','products.id')
    //      ->where('cart.user_id',$userId)
    //      ->sum('products.price');
 
    //      return view('ordernow',['total'=>$total]);
    // }
    function Passeorder(Request $req)
    {
         $userId=Session::get('user')['id'];
         $UserCart= Cart::where('user_id',$userId)->get();
         foreach($UserCart as $cart)
         {
             $order= new Order;
             $order->livre_id=$cart['livre_id'];
             $order->user_id=$cart['user_id'];
             $order->quantite=$cart['quantite'];
             $order->statut="en cours";
             $order->adress="pending";
             $order->save();
             Cart::where('user_id',$userId)->delete(); 
         }
         $req->input();
         $req->session()->put('info', 'la commande est bien passée.');
         return redirect('/');
    }
    function orders()
    {
        $userId=Session::get('user')['id'];
        $orders= DB::table('orders')
         ->join('livres','orders.livre_id','=','livres.id')
         ->where('orders.user_id',$userId)
         ->get();
         return view('orders',['orders'=>$orders]);
    }
    function allorders()
    {
        $orders= DB::table('orders')
         ->join('livres','orders.livre_id','=','livres.id')
         ->select('orders.*','orders.id as order_id','livres.*')
         ->get();   
         return view('allorders',['orders'=>$orders]);
    }
    public function updateStatus($id)
{   $order =Order::find($id);
  if($order->statut == "livré"){$order->statut = "en cours";}
  else{$order->statut = "livré";}
     $order->save();
     return redirect('allorders');}
    
}

