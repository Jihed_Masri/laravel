<?php

namespace App\Http\Controllers;
use Session; 
use Illuminate\Http\Request;
use App\Models\{Livre, Author, Category};
use App\Http\Requests\Category as CategoryRequest;
use Brian2694\Toastr\Facades\Toastr;
class CategoryAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
          $categories = category::all();
        
        if(session()->has('info'))
{
    toastr()->success(session('info'));session()->forget('info');
} return view('category/index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,CategoryRequest $categoryRequest)
    { 
       Category::create($categoryRequest->all());
         $categoryRequest->session()->put('info', 'Le categorie a bien été créé');
         return redirect()->route('categories.index');
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function show(Category $category)
{
return view('category/show', compact('category'));

}
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
        {
        return view('category/edit', compact('category'));
        }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $categoryRequest, Category $category)
{
$category->update($categoryRequest->all());
$categoryRequest->session()->put('info', 'Le categorie a bien été modifié');
return redirect()->route('categories.index');
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Category $category)
    {
        $category->delete();
        $request->session()->put('info', 'Le categorie a bien été supprimé dans la base de données.');
        return redirect()->route('categories.index');

    }
}