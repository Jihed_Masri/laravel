<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLivresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livres', function (Blueprint $table) {
            $table->id();            
            $table->string("name");
            $table->string("price");
            $table->year('year'); 
            $table->string("description");
            $table->string("gallery");
            $table->timestamps();
            $table->unsignedBigInteger('Author_id');
            $table->foreign('Author_id')
            ->references('id')
            ->on('authors')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            Schema::disableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livres');
    }
}
